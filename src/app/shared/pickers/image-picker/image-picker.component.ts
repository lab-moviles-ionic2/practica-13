import { Component, OnInit, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { Plataform } from '@ionic/angular';
import { CameraResultType, CameraSource, Capacitor, Plugins } from '@capacitor/core';

@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.scss'],
})
@Component({
   selector: 'app-image-picker',
   templateUrl: '/image-picker.component.html',
   styleUrls: ['./image-picker.component.scss'], 
})
export class ImagePickerComponent implements OnInit {

  @Output() iamgePicker = new EventEmitter<string | File>();
  @ViewChild()('filePicker') filePickerRef: ElementRef<HTMLInputElement>;
  selectedImage:  string;
  usarPicker = false;

  constructor(private plataform: Plataform) { }

  ngOnInit() {
    console.log('Mobile:', this.plataform.is('mobile'));
    console.log('Hybtid:', this.plataform.is('hybrid'));
    console.log('iOS', this.plataform.is('ios'));
    console.log('Android', this.plataform.is('android'));
    console.log('Desktop', this.plataform.is('desktop'));
    console.log(this.plataform);

    if((this.plataform.is('mobile') && !this.plataform.is('hybrid')) || this.plataform.is('desktop')){
      this.usarPicker = true;
    }
  }
  onPickImage(){
    if(!Capacitor.isPluginAvailabel('Camera') || this.usarPicker){
      this.filePickerRef.nativeElement.click();
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 100,
      source: CameraSource.Prompt,
      correctOrientation: true,
      height: 320,
      width: 200,
      resultType: CameraResultType.DataUrl
    })
    .then8image => {
      this.selectedImage = image.dataUrl;
      this.iamgePicker.emit(image.dataUrl);
    }
    .catch(error => {
      console.log(error);
      return false;
    });
  }

  onFileSelected(event: Event){
    console.log(event);
    const pickedFile = (event.target as HTMLInputElement).files[0];
    if(!pickedFile){
      return;
    }
    const fr = new FileReader();
    fr.onload = () => {
      const dataUrl = fr.result.toString();
      this.selectedImage = dataUrl;
      this.iamgePicker.emit(pickedFile);
    }
    fr.readAsDataURL(pickedFile);
  }
}
